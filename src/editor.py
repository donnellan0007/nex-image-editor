import PySimpleGUI as sg
from tkinter import filedialog
from tkinter import *
import pyautogui
import threading
from PIL import Image,ImageDraw,ImageFilter,ImageFont

# MODULE IMPORTS
# import cropmod

root = Tk()
root.withdraw()
filename = filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("PNG Files","*.png"),("JPEGS Are Coming Soon","*Almost Ready!")))
root.destroy()


# SETTINGS
sg.theme('LightGrey1')

layout = [
    [sg.FileBrowse(key='-FILENAME-')],
    [sg.Text("Crop Image | Width x Height"),sg.InputText(key='-INP-')],
    [sg.Button("Apply"),sg.Button("Cancel")],
    [sg.Image(filename,key='-IMG-')]
]

# Create window
window = sg.Window("Nex Image Editor",layout).Finalize()
window.Maximize()

# event loop process events and values of inputs
def start_gui():
    while True:
        event,values = window.read()
        global img  
        img = {values["-INP-"]}
        img = str(img)
        if event in (None, "Cancel"):
            break
        img.strip()
        if img == '':
            print("Can't be empty")
        else:
            print(crop(0,0))
            # print(f"Cropped image: {values[0]}")

    window.close()

def crop(x,y):
    event,values = window.read()
    # img = {values["-INP-"]}
    # img = str(img)
    global img; return img

if __name__ == "__main__":
    threading.Thread(target=start_gui()).start()